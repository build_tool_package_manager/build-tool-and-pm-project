FROM openjdk:latest

WORKDIR /usr/app

COPY ./build/libs/build-tools-exercises-1.0-SNAPSHOT.jar /usr/app

EXPOSE 8080

ENTRYPOINT ["java","-jar","build-tools-exercises-1.0-SNAPSHOT.jar"]